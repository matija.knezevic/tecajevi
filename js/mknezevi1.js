/**
 * Dohvaca se (AJAX, jSON) konvertirana vrijednost te se postavlja u polje vrijednost2
 */

jQuery( document ).ready(function($) {
    $('#formaValute').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var actionUrl = form.attr('action');
        $.ajax({
            type: 'post',
            url: actionUrl,
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
                console.log(data.data.konvertiranaVrijednost);
                $('#vrijednost2').val(data.data.konvertiranaVrijednost);
            }
        });
    });
});

