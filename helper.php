<?php
class ModNoviHelper
{
    public static $dan;

    /**
     * metoda prima unesene podatke, dohvaca podatke o tecaju te izracunava
     * konvertiranu vrijednost druge valute.
     *
     * @return mixed
     * @throws Exception
     */
    public static function konverzijaAjax()
    {
        $jinput = JFactory::getApplication()->input;
        $vrijednost1 = $jinput->get('vrijednost1');
        $valuta1 = $jinput->get('valuta1');
        $valuta2 = $jinput->get('valuta2');

        $module = JModuleHelper::getModule('mod_novi');
        $params = new JRegistry();
        $params->loadString($module->params);
        $podaci = self::dajTecaj();

        $srednjiValuta1 = $podaci[$valuta1][$params->get('defaulttecaj','')];
        $srednjiValuta2 = $podaci[$valuta2][$params->get('defaulttecaj','')];
        $data['konvertiranaVrijednost'] = round($vrijednost1*$srednjiValuta1/$srednjiValuta2,$params->get('decimale',''));

        return $data;
    }

    /**
     * metoda dohvaca, prociscava i stvara asocijativni niz od podataka o tecaju.
     * Te podatke dohvaca iz cache-a ako je moguce, a ako nije onda sa servera HNB-a.
     *
     * @return array
     */
    public static function dajTecaj(){
        $tecajLista = array();

        if(!file_exists('tecaj.txt') || strtotime("today")!=strtotime(date("Y-m-d",filemtime('tecaj.txt')))) {
            for($i=0; $i<3; $i++) {
                self::$dan = date(d)-$i;
                if($podaci = file_get_contents('http://www.hnb.hr/tecajn/f'.self::$dan.date('my').'.dat',FILE_IGNORE_NEW_LINES)){
                    file_put_contents ('tecaj.txt',$podaci);
                    break;
                }}
        }

        $podaci = file('tecaj.txt',FILE_IGNORE_NEW_LINES);
        array_shift($podaci);
        foreach($podaci as $k => $v) {
            if($k == 0)
                continue;
            $v = explode("      ", $v);
            $valuta = substr($v[0], 3, -3);
            $tecajLista[strtolower($valuta)]['kupovni'] = str_replace(',','.',$v[1]);
            $tecajLista[strtolower($valuta)]['srednji'] = str_replace(',','.',$v[2]);
            $tecajLista[strtolower($valuta)]['prodajni'] = str_replace(',','.',$v[3]);
        }

        return $tecajLista;
    }

    /**
     * metoda formatira ispis svih podataka o tecaju (pregled) na nacin da zamjenjuje
     * '.' sa ',' te ogranicava broj decimala na 5.
     *
     * @param $neformatirani
     * @return string
     */
    public static function formatTecaja($neformatirani){
        $formatirani = number_format($neformatirani,5,',','.');
        return $formatirani;
    }
}
?>