<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_novi
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$doc->addScriptVersion(JUri::root() . '/modules/mod_novi/js/mknezevi1.js');

require_once dirname(__FILE__) . '/helper.php';

if ($params->def('prepare_content', 1))
{
	JPluginHelper::importPlugin('content');
	$module->content = JHtml::_('content.prepare', $module->content, '', 'mod_novi.content');
}

$podaci = array();
$podaci = ModNoviHelper::dajTecaj();
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_novi', $params->get('layout', 'default'));