<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_novi
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="custom<?php echo $moduleclass_sfx ?>" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage');?>)"<?php endif;?> >
    <?php echo $module->content;?>
</div>

<script type="text/javascript" src="js/mknezevi1.js" ></script>

<div>
    <?php
    /**
     * Ispisuje se pregled svih podataka o tecaju. Ispod toga su polja za odabir
     * dvije valute, upis prve vrijednosti te polje za konvertiranu drugu vrijednost
     * koja se izracuna.
     */

    $ispis = '';
    if(!empty($podaci)){
            $ispis = '
                    <table>
                        <tr>
                            <th>VALUTA</th>
                            <th>jed</th>
                            <th>KUPOVNI za devize</th>
                            <th>SREDNJI za devize</th>
                            <th>PRODAJNI za devize</th>
                        </tr>
                ';

            foreach($podaci as $valuta => $tecaj)
            {
                if($params->get($valuta)==1){
                $jedVrijednost = $valuta == 'jpy' || $valuta == 'huf' ? 100 : 1;
                $ispis .= '<tr>';
                $ispis .= '<td>' . $valuta . '</td>';
                $ispis .= '<td>' . $jedVrijednost . '</td>';
                $ispis .= '<td>' . ModNoviHelper::formatTecaja($tecaj['kupovni']) . '</td>';
                $ispis .= '<td>' . ModNoviHelper::formatTecaja($tecaj['srednji']) . '</td>';
                $ispis .= '<td>' . ModNoviHelper::formatTecaja($tecaj['prodajni']) . '</td>';
                $ispis .= '</tr>';
                }
            }
            $ispis .= '</table>';
        echo 'TEČAJNA LISTA OD: ' . (ModNoviHelper::$dan==null ? date(d) : ModNoviHelper::$dan) . ". " . date('m') . ". 20" . date('y') . ".";
        }else{
            $ispis .= 'error: Podaci nisu dohvaceni';
        }
    echo $ispis;
    $ispis = "";
    ?>

    <form id="formaValute" method="post" action="<?php echo JUri::root(true).'/index.php?option=com_ajax&module=novi&format=json&method=konverzija'?>">
        <select name="valuta1">
            <?php
            foreach($podaci as $key=>$value){
                $ispis .= '<option';
                if($key==$valuta1) $ispis .= ' selected=selected';
                $ispis .= ' value="' . $key . '">' . strtoupper($key) . '</option>';
            }
            echo $ispis;
            ?>
        </select>
        <input type="number" id="vrijednost1" name="vrijednost1" value="<?php echo $vrijednost1 ?>"><br>
        <select name="valuta2">
            <?php
            $ispis = '';
            foreach($podaci as $key=>$value){
                $ispis .= '<option';
                if($key==$valuta2) $ispis .= ' selected=selected';
                $ispis .= ' value="' . $key . '">' . strtoupper($key) . '</option>';
            }
            echo $ispis;
            ?>
        </select>
        <input type="number" id="vrijednost2" readonly=true name="vrijednost2" value="<?php echo $vrijednost2 ?>">
        <input type="submit">
    </form>
</div>

